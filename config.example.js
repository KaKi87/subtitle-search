export const
    /**
     * OpenSubtitles data import root directory path
     * @type {string}
     */
    opensubtitlesImportRootPath = undefined,
    /**
     * Database username
     * @type {string}
     */
    dbUsername = undefined,
    /**
     * Database password
     * @type {string}
     */
    dbPassword = undefined,
    /**
     * Database name
     * @type {string}
     */
    dbName = undefined,
    /**
     * Database host
     * @type {string}
     */
    dbHost = 'localhost',
    /**
     * Database port
     * @type {number}
     */
    dbPort = 5432,
    /**
     * API Sentry DSN
     * @type {string}
     */
    apiSentryDsn = undefined,
    /**
     * API port
     * @type {number}
     */
    apiPort = undefined,
    /**
     * Client Sentry DSN
     * @type {string}
     */
    clientSentryDsn = undefined;