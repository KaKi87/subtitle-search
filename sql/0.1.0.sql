CREATE TABLE "srt" (
    "id"                VARCHAR (21),
    "opensubtitlesId"   BIGINT  NOT NULL,
    "isHearingImpaired" BOOLEAN NOT NULL,
    "content"           TEXT    NOT NULL,
    "content_tsvector"  TSVECTOR,
    PRIMARY KEY ("id"),
    UNIQUE ("opensubtitlesId")
);

CREATE INDEX ON "srt" USING gin ("content_tsvector");

CREATE TABLE "series" (
    "id"     VARCHAR (21),
    "imdbId" BIGINT NOT NULL,
    "title"  TEXT   NOT NULL,
    PRIMARY KEY ("id"),
    UNIQUE ("imdbId")
);

CREATE TABLE "season" (
    "id"       VARCHAR (21),
    "seriesId" VARCHAR (21) NOT NULL,
    "number"   INT          NOT NULL,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("seriesId") REFERENCES "series" ("id"),
    UNIQUE ("seriesId", "number")
);

CREATE TABLE "episode" (
    "id"       VARCHAR (21),
    "srtId"    VARCHAR (21) NOT NULL,
    "seasonId" VARCHAR (21) NOT NULL,
    "imdbId"   BIGINT       NOT NULL,
    "number"   INT          NOT NULL,
    "title"    TEXT         NOT NULL,
    "year"     SMALLINT,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("srtId")    REFERENCES "srt"    ("id"),
    FOREIGN KEY ("seasonId") REFERENCES "season" ("id"),
    UNIQUE ("seasonId", "number"),
    UNIQUE ("imdbId")
);

CREATE TABLE "movie" (
    "id"     VARCHAR (21),
    "srtId"  VARCHAR (21) NOT NULL,
    "imdbId" BIGINT       NOT NULL,
    "title"  TEXT         NOT NULL,
    "year"   SMALLINT,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("srtId") REFERENCES "srt" ("id"),
    UNIQUE ("imdbId")
);