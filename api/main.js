import * as Sentry from 'npm:@sentry/node@7.28.1';
import pogo from 'https://deno.land/x/pogo@v0.6.0/main.ts';
import {
    createServerHelper
} from 'https://git.kaki87.net/KaKi87/pogo-helpers/raw/commit/a80ff83ec392106f783f531b37e0f722d65e4d11/mod.js';
import Joi from 'npm:joi@17.6.1';

import db from '../lib/db.js';

import {
    apiSentryDsn,
    apiPort
} from '../config.js';

Sentry.init({
    dsn: apiSentryDsn
});

const
    server = pogo.server({
        port: apiPort
    }),
    {
        createRoute
    } = createServerHelper(
        server,
        {
            isCors: true,
            onError: error => {
                Sentry.captureException(error);
                console.error(error);
            }
        }
    );

createRoute({
    method: 'GET',
    path: '/srt/search',
    schema: Joi.object({
        path: Joi.object(),
        params: Joi.object({
            method: Joi.string().required().valid(
                'to_tsquery',
                'phraseto_tsquery',
                'plainto_tsquery',
                'like',
                'ilike'
            ),
            query: Joi.string().required(),
            movieId: Joi.string(),
            seriesId: Joi.string(),
            seasonId: Joi.string(),
            episodeId: Joi.string()
        })
        .oxor(
            'movieId',
            'seriesId',
            'seasonId',
            'episodeId'
        )
    }),
    handler: async (
        request,
        h,
        {
            params,
            response
        }
    ) => {
        const
            {
                method,
                query,
                movieId,
                seriesId,
                seasonId,
                episodeId
            } = params,
            isTsquery = method.endsWith('_tsquery'),
            {
                rows: [
                    [
                        tsquery
                    ] = []
                ] = []
            } = isTsquery ? await db.queryArray(
                `SELECT ${method}('english', $query)`,
                {
                    query
                }
            ) : {},
            {
                rows
            } = await db.queryArray(`
                SELECT ${isTsquery ? 'ts_rank_cd("content_tsvector", $tsquery) AS score' : '0'}, "srt"."id"
                FROM "srt"
                ${movieId ? 'LEFT JOIN "movie" ON "srt"."id" = "movie"."srtId"' : ''}
                ${seriesId || seasonId || episodeId ? 'LEFT JOIN "episode" ON "srt"."id" = "episode"."srtId"' : ''}
                ${seriesId || seasonId ? 'LEFT JOIN "season" ON "episode"."seasonId" = "season"."id"' : ''}
                ${seriesId ? 'LEFT JOIN "series" ON "season"."seriesId" = "series"."id"' : ''}
                WHERE ${isTsquery ? '"content_tsvector" @@ $tsquery' : `"srt"."content" ${method} $query`}
                ${movieId ? 'AND "movie"."id" = $movieId' : ''}
                ${seriesId ? 'AND "series"."id" = $seriesId' : ''}
                ${seasonId ? 'AND "season"."id" = $seasonId' : ''}
                ${episodeId ? 'AND "episode"."id" = $episodeId' : ''}
                ${isTsquery ? 'ORDER BY score DESC' : ''}
                LIMIT 25;
            `, {
                query: `%${query}%`,
                tsquery,
                movieId,
                seriesId,
                seasonId,
                episodeId
            });
        response.body = {
            tsquery,
            results: rows.map(([
                score,
                id
            ]) => ({
                score: parseFloat(score),
                id
            }))
        };
        return response;
    }
});

createRoute({
    method: 'GET',
    path: '/srt/{id}',
    schema: Joi.object({
        path: Joi.object({
            id: Joi.string().required()
        }),
        params: Joi.object()
    }),
    handler: async (
        request,
        h,
        {
            path,
            response
        }
    ) => {
        const
            {
                id
            } = path,
            {
                rows: [
                    [
                        opensubtitlesId,
                        content,
                        movieId,
                        movieImdbId,
                        movieTitle,
                        movieYear,
                        episodeId,
                        episodeImdbId,
                        episodeNumber,
                        episodeTitle,
                        episodeYear,
                        seasonId,
                        seasonNumber,
                        seriesId,
                        seriesImdbId,
                        seriesTitle
                    ]
                ]
            } = await db.queryArray(`
                SELECT
                    "srt"."opensubtitlesId", "srt"."content",
                    "movie"."id", "movie"."imdbId", "movie"."title", "movie"."year",
                    "episode"."id", "episode"."imdbId", "episode"."number", "episode"."title", "episode"."year",
                    "season"."id", "season"."number",
                    "series"."id", "series"."imdbId", "series"."title"
                FROM "srt"
                LEFT JOIN "movie" ON "srt"."id" = "movie"."srtId"
                LEFT JOIN "episode" ON "srt"."id" = "episode"."srtId"
                LEFT JOIN "season" ON "episode"."seasonId" = "season"."id"
                LEFT JOIN "series" ON "season"."seriesId" = "series"."id"
                WHERE "srt"."id" = $id
            `, {
                id
            }),
            {
                rows: contentTsdebug
            } = await db.queryArray(`
                SELECT "alias" = 'asciiword', "token", "lexemes"
                FROM "srt"
                INNER JOIN ts_debug('english', "srt"."content") ON true
                WHERE "srt"."id" = $id
            `, {
                id
            });
        response.body = {
            opensubtitlesId: Number(opensubtitlesId),
            content,
            contentTsdebug: contentTsdebug.map(([
                isWord,
                token,
                lexemes
            ]) => ({
                isWord,
                token,
                lexemes
            })),
            movieId,
            movieImdbId: movieImdbId && Number(movieImdbId),
            movieTitle,
            movieYear,
            episodeId,
            episodeImdbId: episodeImdbId && Number(episodeImdbId),
            episodeNumber,
            episodeTitle,
            episodeYear,
            seasonId,
            seasonNumber,
            seriesId,
            seriesImdbId: seriesImdbId && Number(seriesImdbId),
            seriesTitle
        };
        return response;
    }
});

createRoute({
    method: 'GET',
    path: '/search',
    schema: Joi.object({
        path: Joi.object(),
        params: Joi.object({
            query: Joi.string().required()
        })
    }),
    handler: async (
        request,
        h,
        {
            params,
            response
        }
    ) => {
        const
            query = `%${params['query']}%`,
            {
                rows: movieRows
            } = await db.queryArray(`
                SELECT "movie"."id", "movie"."srtId", "movie"."imdbId", "movie"."title", "movie"."year"
                FROM "movie"
                WHERE "movie"."title" ILIKE $query
                LIMIT 100
            `, {
                query
            }),
            {
                rows: episodeRows
            } = await db.queryArray(`
                SELECT
                    "episode"."id", "episode"."srtId", "episode"."imdbId", "episode"."title", "episode"."year", "episode"."number",
                    "season"."id", "season"."number",
                    "series"."id", "series"."imdbId", "series"."title"
                FROM "episode"
                INNER JOIN "season" ON "episode"."seasonId" = "season"."id"
                INNER JOIN "series" ON "season"."seriesId" = "series"."id"
                WHERE "episode"."title" ILIKE $query
                LIMIT 100
            `, {
                query
            }),
            {
                rows: seriesRows
            } = await db.queryArray(`
                SELECT "series"."id", "series"."imdbId", "series"."title"
                FROM "series"
                WHERE "series"."title" ILIKE $query
                LIMIT 100
            `, {
                query
            });
        response.body = {
            movies: movieRows.map(([
                id,
                srtId,
                imdbId,
                title,
                year
            ]) => ({
                id,
                srtId,
                imdbId: Number(imdbId),
                title,
                year
            })),
            episodes: episodeRows.map(([
                id,
                srtId,
                imdbId,
                title,
                year,
                number,
                seasonId,
                seasonNumber,
                seriesId,
                seriesImdbId,
                seriesTitle
            ]) => ({
                id,
                srtId,
                imdbId: Number(imdbId),
                title,
                year,
                number,
                seasonId,
                seasonNumber,
                seriesId,
                seriesImdbId: Number(seriesImdbId),
                seriesTitle
            })),
            series: seriesRows.map(([
                id,
                imdbId,
                title
            ]) => ({
                id,
                imdbId: Number(imdbId),
                title
            }))
        };
        return response;
    }
});

createRoute({
    method: 'GET',
    path: '/series/{id}/episodes',
    schema: Joi.object({
        path: Joi.object({
            id: Joi.string().required()
        }),
        params: Joi.object()
    }),
    handler: async (
        request,
        h,
        {
            path,
            response
        }
    ) => {
        const
            {
                id
            } = path,
            {
                rows
            } = await db.queryArray(`
                SELECT
                    "episode"."id", "episode"."srtId", "episode"."imdbId", "episode"."title", "episode"."year", "episode"."number",
                    "season"."id", "season"."number",
                    "series"."id", "series"."imdbId", "series"."title"
                FROM "episode"
                INNER JOIN "season" ON "episode"."seasonId" = "season"."id"
                INNER JOIN "series" ON "season"."seriesId" = "series"."id"
                WHERE "series"."id" = $id
            `, {
                id
            });
        response.body = rows.map(([
            id,
            srtId,
            imdbId,
            title,
            year,
            number,
            seasonId,
            seasonNumber,
            seriesId,
            seriesImdbId,
            seriesTitle
        ]) => ({
            id,
            srtId,
            imdbId: Number(imdbId),
            title,
            year,
            number,
            seasonId,
            seasonNumber,
            seriesId,
            seriesImdbId: Number(seriesImdbId),
            seriesTitle
        }));
        return response;
    }
});

createRoute({
    method: 'GET',
    path: '/discord',
    handler: (
        request,
        h,
        {
            response
        }
    ) => {
        response.redirect('https://discord.com/invite/TdcuByN5at');
        return response;
    }
});

server.start();