import { Client } from 'https://deno.land/x/postgres@v0.17.0/mod.ts';

import {
    dbUsername,
    dbPassword,
    dbName,
    dbHost,
    dbPort
} from '../config.js';

export default new Client({
    user: dbUsername,
    password: dbPassword,
    database: dbName,
    hostname: dbHost,
    port: dbPort
});