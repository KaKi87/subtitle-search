if(process.env.NODE_ENV === 'development'){
    globalThis.__VUE_OPTIONS_API__ = true
    globalThis.__VUE_PROD_DEVTOOLS__ = false;
}

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import axios from 'axios';
import { createEmitter } from 'simpler-emitter';
import * as Sentry from '@sentry/vue';

import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import 'primevue/resources/primevue.min.css';
import 'primevue/resources/themes/bootstrap4-dark-purple/theme.css';
import 'primeicons/primeicons.css';

import App from './components/App.vue';
import Home from './components/Main/Home.vue';
import Srt from './components/Main/Srt.vue';

import {
    clientSentryDsn
} from '../../config.js';

const
    app = createApp(App),
    router = createRouter({
        history: createWebHistory(),
        routes: [
            {
                path: '/',
                name: 'home',
                component: Home
            },
            {
                path: '/srt/:id',
                name: 'srt',
                component: Srt
            }
        ]
    }),
    client = axios.create({
        baseURL: 'https://api.subtitle-search.kaki87.net'
    }),
    emitter = createEmitter();

if(clientSentryDsn) Sentry.init({
    app,
    dsn: clientSentryDsn,
    logErrors: true,
    beforeSend: event => {
        if(event.exception){
            emitter.emit(
                'exceptionCaptured',
                {
                    event,
                    showReportDialog: () => Sentry.showReportDialog({ eventId: event.event_id })
                }
            );
        }
        return event;
    }
});

app.use(router);

app.use(PrimeVue);
app.use(ToastService);

app.config.globalProperties.client = client;
app.config.globalProperties.emitter = emitter;

app.mount('.App');
