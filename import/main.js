import opensubtitlesImport from 'https://git.kaki87.net/KaKi87/opensubtitles-import/raw/branch/master/mod.js';
import { nanoid } from 'https://deno.land/x/nanoid@v3.0.0/mod.ts';
import { DOMParser } from 'https://deno.land/x/deno_dom/deno-dom-wasm.ts';

import db from '../lib/db.js';
import { opensubtitlesImportRootPath } from '../config.js';

let i = 0;

await opensubtitlesImport({
    rootPath: opensubtitlesImportRootPath,
    onItem: async ({
        item,
        srtContent
    }) => {
        const
            opensubtitlesId = parseInt(item['IDSubtitle']),
            {
                rows: [[srtId]]
            } = await db.queryArray(`
                INSERT INTO "srt" ("id", "opensubtitlesId", "isHearingImpaired", "content")
                VALUES ($id, $opensubtitlesId, $isHearingImpaired, $content)
                ON CONFLICT ("opensubtitlesId")
                DO UPDATE SET "opensubtitlesId" = EXCLUDED."opensubtitlesId"
                RETURNING "id";
            `, {
                id: nanoid(),
                opensubtitlesId,
                isHearingImpaired: Boolean(item['SubHearingImpaired']),
                content: new DOMParser().parseFromString(srtContent,'text/html').textContent
                    .replace(/^\d+$/gm, '')
                    .replace(/^(\d{2}:){2}\d{2},\d{3} --> (\d{2}:){2}\d{2},\d{3}$/gm, '')
                    .replace(/{\an[0-9]}/g, '')
                    .replace(/\n+/g, ' ')
            });
        try {
            await db.queryArray(`
                UPDATE "srt"
                SET "content_tsvector" = to_tsvector('english', "content")
                WHERE "id" = $srtId
            `, {
                srtId
            });
        }
        catch(error){
            console.error(error);
        }
        if(!item['SeriesIMDBParent'] || item['SeriesIMDBParent'] === '0'){
            await db.queryArray(`
                INSERT INTO "movie" ("id", "srtId", "imdbId", "title", "year")
                VALUES ($id, $srtId, $imdbId, $title, $year)
                ON CONFLICT ("imdbId") DO NOTHING
            `, {
                id: nanoid(),
                srtId,
                imdbId: parseInt(item['MovieImdbID']),
                title: item['MovieName'],
                year: item['MovieYear'] ? parseInt(item['MovieYear']) : null
            });
        }
        else {
            const
                [seriesTitle, episodeTitle] = item['MovieName'].slice(1).split('" '),
                {
                    rows: [[seriesId]]
                } = await db.queryArray(`
                    INSERT INTO "series" ("id", "imdbId", "title")
                    VALUES ($id, $imdbId, $title)
                    ON CONFLICT ("imdbId")
                    DO UPDATE SET "imdbId" = EXCLUDED."imdbId"
                    RETURNING "id";
                `, {
                    id: nanoid(),
                    imdbId: parseInt(item['SeriesIMDBParent']),
                    title: seriesTitle
                }),
                {
                    rows: [[seasonId]]
                } = await db.queryArray(`
                    INSERT INTO "season" ("id", "seriesId", "number")
                    VALUES ($id, $seriesId, $number)
                    ON CONFLICT ("seriesId", "number")
                    DO UPDATE SET "seriesId" = EXCLUDED."seriesId"
                    RETURNING "id";
                `, {
                    id: nanoid(),
                    seriesId,
                    number: parseInt(item['SeriesSeason'])
                });
            await db.queryArray(`
                INSERT INTO "episode" ("id", "srtId", "seasonId", "imdbId", "number", "title", "year")
                VALUES ($id, $srtId, $seasonId, $imdbId, $number, $title, $year)
                ON CONFLICT ("seasonId", "number") DO NOTHING
            `, {
                id: nanoid(),
                srtId,
                seasonId,
                imdbId: parseInt(item['MovieImdbID']),
                number: parseInt(item['SeriesEpisode']),
                title: episodeTitle,
                year: item['MovieYear'] ? parseInt(item['MovieYear']) : null
            });
        }
        console.log(++i, opensubtitlesId);
    }
});